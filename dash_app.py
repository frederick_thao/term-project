import dash

from functions import make_layout


app = dash.Dash(__name__)

app.layout = make_layout()

if __name__ == "__main__":

    app.run_server(debug=True)