import pandas as pd
from dash import html
from dash import dcc
import os


def get_death_data():
    filepath = os.path.join(os.getcwd(), "Provisional_COVID-19_Death_Counts_by_Week_Ending_Date_and_State.csv")
    data = pd.read_csv(filepath)

    death_data = data.query("Group == 'By Week' and State == 'United States'").copy()

    return death_data


def get_vaccine_data():
    filepath = os.path.join(os.getcwd(), "COVID-19_Vaccinations_in_the_United_States_Jurisdiction.csv")
    data = pd.read_csv(filepath, low_memory=False)

    vaccine_data = data.query("Location == 'US'")
    vaccine_data = vaccine_data.iloc[::-1]

    return vaccine_data



def make_layout():
    d_data = get_death_data()
    v_data = get_vaccine_data()
    layout = html.Div(

        children=[

            html.H1(children="COVID Analytics", ),

            html.P(

                children="Analyze the behavior of COVID Deaths"

                         " and Vaccines Administered"

                         " between 2020 and 2022",

            ),

            dcc.Graph(

                figure={

                    "data": [

                        {

                            "x": d_data["Start Date"],

                            "y": d_data["COVID_Deaths"],

                            "type": "lines",

                        },

                    ],

                    "layout": {"title": "Number of Deaths in the U.S."},

                },

            ),

            dcc.Graph(

                figure={

                    "data": [

                        {

                            "x": v_data["Date"],

                            "y": v_data["Administered_Total"],

                            "type": "lines",

                        },

                    ],

                    "layout": {"title": "Total Administered Vaccines"},

                },

            ),

            dcc.Graph(

                figure={

                    "data": [

                        {

                            "x": v_data["Date"],

                            "y": v_data["Administered_Janssen"],

                            "type": "lines",

                        },

                    ],

                    "layout": {"title": "Total Administered Janssen Vaccine"},

                },

            ),

            dcc.Graph(

                figure={

                    "data": [

                        {

                            "x": v_data["Date"],

                            "y": v_data["Administered_Pfizer"],

                            "type": "lines",

                        },

                    ],

                    "layout": {"title": "Total Administered Pfizer Vaccine"},

                },

            ),

            dcc.Graph(

                figure={

                    "data": [

                        {

                            "x": v_data["Date"],

                            "y": v_data["Administered_Moderna"],

                            "type": "lines",

                        },

                    ],

                    "layout": {"title": "Total Administered Moderna Vaccines"},

                },

            ),
        ]

    )
    return layout
